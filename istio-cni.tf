locals {
  istio_cni_name      = "istio-cni"
  istio_cni_namespace = "kube-system"
}

resource "helm_release" "istio_cni" {
  count = local.install_istio ? 1 : 0

  name      = local.istio_cni_name
  chart     = "${local.istio_helm_repository_name}/${local.istio_cni_name}"
  namespace = local.istio_cni_namespace
  version   = var.istio_helm_chart_version

  timeout = 1200

  values = [
    join("", data.template_file.istio_cni.*.rendered),
  ]

  depends_on = [
    kubernetes_deployment.tiller_deploy,
    helm_release.istio_init,
  ]
}


data "template_file" "istio_cni" {
  count = local.install_istio ? 1 : 0

  template = file("${local.files_path}/templates/istio/${local.istio_cni_name}-values.yaml.tpl")
}
