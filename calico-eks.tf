# Reference: https://docs.aws.amazon.com/eks/latest/userguide/calico.html
locals {
  aws_vpc_cni_calico_manifest_file = join("", local_file.aws_k8s_cni_calico_manifest.*.filename)
}

data "http" "aws_k8s_cni_calico_manifest" {
  count = var.upgrade_aws_k8s_cni_plugin ? 1 : 0

  url = var.aws_k8s_cni_calico_manifest_url
}

resource "local_file" "aws_k8s_cni_calico_manifest" {
  count = var.upgrade_aws_k8s_cni_plugin ? 1 : 0

  filename = "${var.config_output_path}/${var.aws_k8s_cni_calico_manifest_file}"
  content  = join("", data.http.aws_k8s_cni_calico_manifest.*.body)
}

resource "null_resource" "calico_eks_resources" {
  count = local.calico_with_eks ? 1 : 0

  provisioner "local-exec" {
    command = "kubectl apply -f ${local.aws_vpc_cni_calico_manifest_file} --kubeconfig ${var.kubeconfig_file}"
  }

  triggers = {
    calico_config = sha1(join("", local_file.aws_k8s_cni_calico_manifest.*.content))
  }
}

resource "null_resource" "calico_eks_resources_destroy" {
  count = local.calico_with_eks ? 1 : 0

  provisioner "local-exec" {
    when    = destroy
    command = "kubectl delete -f ${local.aws_vpc_cni_calico_manifest_file} --kubeconfig ${var.kubeconfig_file}"
  }
}
