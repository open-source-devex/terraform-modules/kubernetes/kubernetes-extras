resource "kubernetes_daemonset" "calico" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name      = "calico-node"
    namespace = "kube-system"

    labels = {
      k8s-app = "calico-node"
    }
  }

  spec {
    selector {
      match_labels = {
        k8s-app = "calico-node"
      }
    }

    strategy {
      type = "RollingUpdate"

      rolling_update {
        max_unavailable = "1"
      }
    }

    template {
      metadata {
        labels = {
          k8s-app = "calico-node"
        }

        annotations = {}
        # This, along with the CriticalAddonsOnly toleration below,  # marks the pod as a critical add-on, ensuring it gets  # priority scheduling and that its resources are reserved  # if it ever gets evicted.  #scheduler.alpha.kubernetes.io/critical-pod" = ""
      }

      spec {
        host_network         = true
        service_account_name = kubernetes_service_account.calico_node[0].metadata[0].name

        # Minimize downtime during a rolling upgrade or deletion; tell Kubernetes to do a "force
        # deletion": https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods.
        termination_grace_period_seconds = 0

        container {
          # Runs calico/node container on each Kubernetes node.  This
          # container programs network policy and routes on each
          # host.
          name = "calico-node"

          image             = "quay.io/calico/node:v3.1.3"
          image_pull_policy = "IfNotPresent"

          env {
            name  = "DATASTORE_TYPE"
            value = "kubernetes"
          }

          env {
            # Use eni not cali for interface prefix
            name  = "FELIX_INTERFACEPREFIX"
            value = "eni"
          }

          env {
            # Enable felix info logging.
            name  = "FELIX_LOGSEVERITYSCREEN"
            value = "info"
          }

          env {
            # Don't enable BGP.
            name  = "CALICO_NETWORKING_BACKEND"
            value = "none"
          }

          env {
            # Cluster type to identify the deployment type
            name  = "CLUSTER_TYPE"
            value = "k8s,ecs"
          }

          env {
            # Disable file logging so `kubectl logs` works.
            name  = "CALICO_DISABLE_FILE_LOGGING"
            value = "true"
          }

          env {
            name  = "FELIX_TYPHAK8SSERVICENAME"
            value = "calico-typha"
          }

          env {
            # Set Felix endpoint to host default action to ACCEPT.
            name  = "FELIX_DEFAULTENDPOINTTOHOSTACTION"
            value = "ACCEPT"
          }

          env {
            # This will make Felix honor AWS VPC CNI's mangle table
            # rules.
            name = "FELIX_IPTABLESMANGLEALLOWACTION"

            value = "Return"
          }

          env {
            # Disable IPV6 on Kubernetes.
            name  = "FELIX_IPV6SUPPORT"
            value = "false"
          }

          env {
            # Wait for the datastore.
            name  = "WAIT_FOR_DATASTORE"
            value = "true"
          }

          env {
            name  = "FELIX_LOGSEVERITYSYS"
            value = "none"
          }

          env {
            name  = "FELIX_PROMETHEUSMETRICSENABLED"
            value = "true"
          }

          env {
            name  = "NO_DEFAULT_POOLS"
            value = "true"
          }

          env {
            name  = "FELIX_HEALTHENABLED"
            value = "true"
          }

          env {
            # No IP address needed.
            name = "IP"
          }

          env {
            name = "NODENAME"

            value_from {
              field_ref {
                field_path = "spec.nodeName"
              }
            }
          }

          # Use Kubernetes API as the backing datastore.
          security_context {
            privileged = true
          }

          liveness_probe {
            http_get {
              path   = "/liveness"
              port   = 9099
              host   = "localhost"
              scheme = "HTTP"
            }

            period_seconds        = 10
            initial_delay_seconds = 10
            failure_threshold     = 6
            success_threshold     = 1
          }

          readiness_probe {
            http_get {
              path   = "/readiness"
              port   = "9099"
              scheme = "HTTP"
            }

            success_threshold = 1
            period_seconds    = 10
          }

          volume_mount {
            mount_path = "/lib/modules"
            name       = "lib-modules"
            read_only  = true
          }

          volume_mount {
            mount_path = "/var/run/calico"
            name       = "var-run-calico"
            read_only  = false
          }
        }

        volume {
          name = "lib-modules"

          host_path {
            path = "/lib/modules"
          }
        }

        volume {
          name = "var-run-calico"

          host_path {
            path = "/var/run/calico"
          }
        }
      }
    }
  }
}

resource "kubernetes_service_account" "calico_node" {
  count = local.calico_without_eks ? 1 : 0

  automount_service_account_token = true

  metadata {
    name      = "calico-node"
    namespace = "kube-system"
  }
}

resource "kubernetes_cluster_role" "calico_node" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name = "calico-node"
  }

  rule {
    api_groups = [""]
    resources  = ["namespaces"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    api_groups = [""]
    resources  = ["pods/status"]
    verbs      = ["update"]
  }

  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list", "watch", "patch"]
  }

  rule {
    api_groups = [""]
    resources  = ["services"]
    verbs      = ["get"]
  }

  rule {
    api_groups = [""]
    resources  = ["endpoints"]
    verbs      = ["get"]
  }

  rule {
    api_groups = [""]
    resources  = ["nodes"]
    verbs      = ["get", "list", "update", "watch"]
  }

  rule {
    api_groups = ["extensions"]
    resources  = ["networkpolicies"]
    verbs      = ["get", "list", "watch"]
  }

  rule {
    api_groups = ["networking.k8s.io"]
    resources  = ["networkpolicies"]
    verbs      = ["list", "watch"]
  }

  rule {
    api_groups = ["crd.projectcalico.org"]

    resources = [
      "globalfelixconfigs",
      "felixconfigurations",
      "bgppeers",
      "globalbgpconfigs",
      "bgpconfigurations",
      "ippools",
      "globalnetworkpolicies",
      "globalnetworksets",
      "networkpolicies",
      "clusterinformations",
      "hostendpoints",
    ]

    verbs = ["create", "get", "list", "update", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "calico_node" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name = "calico-node"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "calico-node"
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.calico_node[0].metadata[0].name
    namespace = "kube-system"
  }
}

resource "kubernetes_deployment" "calico_typha" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name      = "calico-typha"
    namespace = "kube-system"

    labels = {
      k8s-app = "calico-typha"
    }
  }

  spec {
    revision_history_limit = 2
    replicas               = 2

    selector {
      match_labels = {
        k8s-app = "calico-typha"
      }
    }

    template {
      metadata {
        namespace = ""

        labels = {
          k8s-app = "calico-typha"
        }
      }

      spec {
        host_network         = true
        service_account_name = kubernetes_service_account.calico_node[0].metadata[0].name

        container {
          image = "quay.io/calico/typha:v0.7.4"
          name  = "calico-typha"

          port {
            container_port = 5473
            host_port      = 5473
            name           = "calico-typha"
            protocol       = "TCP"
          }

          env {
            name  = "FELIX_INTERFACEPREFIX"
            value = "eni"
          }

          env {
            name  = "TYPHA_LOGFILEPATH"
            value = "none"
          }

          env {
            name  = "TYPHA_LOGSEVERITYSYS"
            value = "none"
          }

          env {
            name  = "TYPHA_LOGSEVERITYSCREEN"
            value = "info"
          }

          env {
            name  = "TYPHA_PROMETHEUSMETRICSENABLED"
            value = "true"
          }

          env {
            name  = "TYPHA_CONNECTIONREBALANCINGMODE"
            value = "kubernetes"
          }

          env {
            name  = "TYPHA_PROMETHEUSMETRICSPORT"
            value = "9093"
          }

          env {
            name  = "TYPHA_DATASTORETYPE"
            value = "kubernetes"
          }

          env {
            name  = "TYPHA_MAXCONNECTIONSLOWERLIMIT"
            value = "1"
          }

          env {
            name  = "TYPHA_HEALTHENABLED"
            value = "true"
          }

          # This will make Felix honor AWS VPC CNI's mangle tabl rules.
          env {
            name  = "FELIX_IPTABLESMANGLEALLOWACTION"
            value = "Return"
          }

          volume_mount {
            mount_path = "/etc/calico"
            name       = "etc-calico"
            read_only  = true
          }

          liveness_probe {
            http_get {
              path = "/liveness"
              port = 9098
            }

            period_seconds        = 30
            initial_delay_seconds = 30
          }

          readiness_probe {
            http_get {
              path = "/readiness"
              port = 9098
            }

            period_seconds = 10
          }
        }

        volume {
          name = "etc-calico"

          host_path {
            path = "/etc/calico"
          }
        }
      }
    }
  }
}

resource "null_resource" "calico_customrecources" {
  count = local.calico_without_eks ? 1 : 0

  provisioner "local-exec" {
    command = "kubectl apply -f ${path.module}/files/calico/calico.yaml --kubeconfig ${var.kubeconfig_file}"
  }

  provisioner "local-exec" {
    when    = destroy
    command = "kubectl delete -f ${path.module}/files/calico/calico.yaml --kubeconfig ${var.kubeconfig_file}"
  }
}

