# 2019/10/11 - ref: https://docs.aws.amazon.com/eks/latest/userguide/cni-upgrades.html
locals {
  aws_k8s_cni_manifest_file = join("", local_file.aws_k8s_cni_manifest.*.filename)

  cni_plugin_namespace = "kube-system"
  cni_plugin_name      = "aws-node"
}

data "http" "aws_k8s_cni_manifest" {
  count = var.upgrade_aws_k8s_cni_plugin ? 1 : 0

  url = var.aws_k8s_cni_manifest_url
}

resource "local_file" "aws_k8s_cni_manifest" {
  count = var.upgrade_aws_k8s_cni_plugin ? 1 : 0

  filename = "${var.config_output_path}/${var.aws_k8s_cni_manifest_file}"
  content  = join("", data.http.aws_k8s_cni_manifest.*.body)
}

resource "null_resource" "upgrade_aws_k8s_cni_plugin" {
  count = var.upgrade_aws_k8s_cni_plugin ? 1 : 0

  provisioner "local-exec" {
    when    = create
    command = "kubectl apply -f ${local.aws_k8s_cni_manifest_file} --kubeconfig ${var.kubeconfig_file}"
  }

  triggers = {
    calico_config = sha1(join("", local_file.aws_k8s_cni_manifest.*.content))
  }
}

resource "null_resource" "upgrade_aws_k8s_cni_plugin_destroy" {
  count = var.upgrade_aws_k8s_cni_plugin ? 1 : 0

  provisioner "local-exec" {
    when    = destroy
    command = "kubectl delete -f ${local.aws_k8s_cni_manifest_file} --kubeconfig ${var.kubeconfig_file}"
  }
}

# When we upgrade de CNI plugin the serivice account is recreated and the IAM OIDC annotation is lost.
# This module will trigger when the upgrade happens and it will re-annotate the service account.
module "iam_role_for_cni" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/iam-role-for-k8s-service-account?ref=v1.0.9"

  project     = var.project
  environment = var.environment
  tags        = var.tags

  create           = var.upgrade_aws_k8s_cni_plugin
  create_iam_role  = false
  external_trigger = sha1(join("", local_file.aws_k8s_cni_manifest.*.content))

  aws_account_id    = var.aws_account_id
  aws_region        = var.aws_region
  aws_iam_role_name = var.cni_iam_role_name

  kubeconfig_file           = var.kubeconfig_file
  cluster_oidc_provider_url = var.oidc_provider_url

  k8s_service_account_namespace = local.cni_plugin_namespace
  k8s_service_account_name      = local.cni_plugin_name
  k8s_fully_defined_resources   = ["daemonset.apps/${local.cni_plugin_name}"]
  aws_iam_role_policy_arns      = ["arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"]
}
