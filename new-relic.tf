data "template_file" "new_relic" {
  count = var.install_new_relic ? 1 : 0

  template = file(
    "${local.files_path}/templates/new-relic/newrelic-infrastructure-k8s-latest.yaml.tmpl",
  )

  vars = {
    cluster_name     = var.cluster_name
    nria_license_key = var.nria_license_key
  }
}

resource "local_file" "new_relic" {
  count = var.install_new_relic ? 1 : 0

  filename = "tmp-new-relic.yaml"
  content  = join("", data.template_file.new_relic.*.rendered)
}

resource "null_resource" "new_relic" {
  count = var.install_new_relic ? 1 : 0

  triggers = {
    content = join("", data.template_file.new_relic.*.rendered)
  }

  provisioner "local-exec" {
    command = "kubectl apply -f tmp-new-relic.yaml --kubeconfig ${var.kubeconfig_file}"
  }

  depends_on = [local_file.new_relic]
}

