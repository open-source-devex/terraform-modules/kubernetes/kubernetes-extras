resource "helm_release" "k8s_spot_termination_handler" {
  count = var.install_ec2_spot_termination_handler ? 1 : 0

  keyring   = ""
  name      = "k8s-spot-termination-handler"
  chart     = "stable/k8s-spot-termination-handler"
  namespace = "kube-system"

  force_update = var.hlem_release_force_update

  set {
    name  = "clusterName"
    value = var.cluster_name
  }

  depends_on = [kubernetes_deployment.tiller_deploy]
}

