locals {
  external_dns_iam_resource_name = "k8s-${local.external_dns_name}-ctrl-${var.project}-${var.environment}-${var.aws_region}"
}

resource "aws_iam_policy" "external_dns" {
  count = var.install_external_dns ? 1 : 0

  name   = local.external_dns_iam_resource_name
  policy = join("", data.aws_iam_policy_document.external_dns_role_policy.*.json)
}

data "aws_iam_policy_document" "external_dns_role_policy" {
  count = var.install_external_dns ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "route53:ListTagsForResources",
      "route53:GetHostedZone",
      "route53:ListTrafficPolicyInstancesByHostedZone",
      "route53:ChangeResourceRecordSets",
      "route53:ListResourceRecordSets",
      "route53:ListVPCAssociationAuthorizations",
      "route53:ListTagsForResource",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "route53:ListTrafficPolicyInstances",
      "route53:ListHostedZones",
      "route53:ListTrafficPolicies",
      "route53:TestDNSAnswer",
      "route53:ListHostedZonesByName",
    ]

    resources = ["*"]
  }
}

##########
# iam-oidc
##########
# since the helm chart for the external-dns controller supports service account annotations we do not need to
# use the module that creates the role an annotates the account (since helm will annotate it for us)
module "external_dns_iam_role" {
  source = "git::https://github.com/miguelaferreira/terraform-aws-iam.git//modules/iam-assumable-role-with-iodc?ref=iam-assumable-role-with-oidc"

  create_role = var.install_external_dns && local.iam_oidc_mode_enabled

  provider_url = var.oidc_provider_url

  role_name        = local.external_dns_iam_resource_name
  role_policy_arns = [join("", aws_iam_policy.external_dns.*.arn)]

  oidc_fully_qualified_subjects = ["system:serviceaccount:${local.external_dns_namespace}:${local.external_dns_name}"]

  tags = var.tags
}

##########
# kube2iam
##########
resource "aws_iam_role" "external_dns_kube2iam" {
  count = var.install_external_dns && local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  name               = local.external_dns_iam_resource_name
  assume_role_policy = join("", data.aws_iam_policy_document.eks_workers_assume_role_kube2iam_trust.*.json)
}

resource "aws_iam_role_policy_attachment" "external_dns_kube2iam" {
  count = var.install_external_dns && local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  role       = join("", aws_iam_role.external_dns_kube2iam.*.name)
  policy_arn = join("", aws_iam_policy.external_dns.*.arn)
}
