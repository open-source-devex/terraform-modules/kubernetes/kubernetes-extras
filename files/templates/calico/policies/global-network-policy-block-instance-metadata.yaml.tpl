apiVersion: crd.projectcalico.org/v1
kind: GlobalNetworkPolicy
metadata:
  name: ${policy_name}
spec:
  selector: all()
  types:
  - Egress
  egress:
  - action: Deny
    protocol: TCP
    source:
      selector: all()
    destination:
      nets:
      - 169.254.169.254/32
  - action: Allow
