# kubernetes-extras

Terraform module to setup k8s extras like calico, istio and others.

## Assigning IAM roles to pods

This module provides two ways of assigning roles to pods. One is to use `kube2iam` and the other is to use OpenId connect.
The former is tested and should work for any pod, and is the default implementation. The latter requires the binaries in the pods to be build against a version of the AWS SDK that
supports assuming roles using web identity.
See [Notes](#notes) bellow for details about support for OpenId Connect of the various services this module installs.

The choice between the modes is done via the `pod_iam_policy_mode` variable.
```hcl
pod_iam_policy_mode = "kube2iam"

# or

pod_iam_policy_mode = "iam-oidc"
```

### Assigning roles to pods using OpenID connect

Documentation here: https://docs.aws.amazon.com/eks/latest/userguide/iam-roles-for-service-accounts.html

When the IAM OIDC mode is enabled we do not want pods to reach the EC2 instance metadata endpoint,
otherwise they can try to assume the worker instance profile, which would stop them from assuming
the role provided via OIDC. The EKS documentation says to add a iptables rule to the worker user data
to block access to the metadata endpoint. However, that does not work with calico because calico
also inserts a rule in the same iptables table, and calico's rule always takes precedence.
Therefore, when calico is installed on EKS we setup a calico GlobalNetworkPolicy to block pods
from accessing the instance metadata endpoint.

One issues with the calico policy to block pod access to the EC2 metadata endpoint is that if the policy gets changed or removed terraform won't put it back.
It would be great to use a calico provider (and there is [one](https://github.com/Cdiscount/terraform-provider-calico) but it's not that much developed).
An alternative is to use an external data source that reads the state of the policy, does the comparison and triggers the policy to be re-applied if it has drifted.

## Notes

The autoscaler project does not yet support the IAM-OIDC method to obtain IAM policies.
There should be a release soon that adds support for it. For now autoscaler is broken when `pod_iam_policy_mode = "iam-oidc"` 
See: https://github.com/kubernetes/autoscaler/issues/2301#issuecomment-541148636

The service broker project does not yet support the IAM-OIDC method to obtain IAM policies.
Eric is prepared a PR to add support for it to the service broker: https://github.com/awslabs/aws-servicebroker/pull/163

Due to old logic that handles destroy time provisioners, uninstalling services by flipping their flag to false might result in a cycle.
In that case, run a plan targeting the resource that destroys the service (topically `kubectl delete -f ...`), then apply that plan before running terraform again over the entire configuration. 
See https://github.com/hashicorp/terraform/issues/21662 for in-depth details on this issue.
