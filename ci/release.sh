#!/usr/bin/env bash

set -ev

create-tag() {
  local version=${1}

  git tag --annotate v${version} --message "Release v${version}"
  git push --tags
}

is-already-tagged() {
  local version=${1}

  [[ $( git rev-list -n 1 v${version} 2> /dev/null ) == $( git rev-parse HEAD ) ]]
}

VERSION_FILE="version.txt"
VERSION=$( cat ${VERSION_FILE} )

if [[  "$( is-already-tagged ${VERSION} )" != 0 ]]; then
  create-tag "${VERSION}"
else
  echo "==> Already tagged with the right version"
fi

if [[ -z "$( git branch | grep '^* master$' )" ]]; then
  # Checkout master
  git branch -d master || true  # the branch might not exists
  git fetch origin master:master
  git checkout master
fi

bump.sh ${VERSION} > ${VERSION_FILE}
git add ${VERSION_FILE}
git commit -m "Incremented version from ${VERSION} to $( cat ${VERSION_FILE} )"
git push origin master
