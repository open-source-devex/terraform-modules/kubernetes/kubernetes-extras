locals {
  install_istio_alb_ingress = local.install_istio && local.install_alb_ingress

  istio_alb_ingress_name = "istio-alb-ingress"

  istio_alb_ingress_manifest_file = "${var.config_output_path}/${var.istio_alb_ingress_manifest_file}"
}

data "template_file" "istio_alb_ingress" {
  count = local.install_istio_alb_ingress ? 1 : 0

  template = file("${local.files_path}/templates/istio/alb-ingress.yaml.tpl")

  vars = {
    name                = local.istio_alb_ingress_name
    namespace           = local.istio_namespace
    app_label           = local.istio_name
    host_name           = var.istio_alb_ingress_external_dns_name
    security_group_ids  = join(",", var.istio_alb_ingress_security_group_ids)
    subnet_ids          = join(",", var.istio_alb_ingress_subnets)
    tls_certificate_arn = var.istio_alb_ingress_tls_certificate_arn
    cognito_config      = var.istio_alb_ingress_enable_cognito_auth ? join("\n", local.alb_auth_cognito) : ""
    tags                = join(",", formatlist("%s=%s", keys(var.tags), values(var.tags)))
    target_type         = var.istio_alb_ingress_target_type
    logs_s3_enabled     = var.istio_alb_ingress_logs_s3_enabled
    logs_s3_bucket      = var.istio_alb_ingress_logs_s3_bucket
    logs_s3_prefix      = var.istio_alb_ingress_logs_s3_object_prefix
  }
}

resource "local_file" "istio_alb_ingress" {
  count = local.install_istio_alb_ingress ? 1 : 0

  filename = local.istio_alb_ingress_manifest_file
  content  = join("", data.template_file.istio_alb_ingress.*.rendered)
}

resource "null_resource" "istio_alb_ingress" {
  count = local.install_istio_alb_ingress ? 1 : 0

  triggers = {
    content = join("", data.template_file.istio_alb_ingress.*.rendered)
  }

  provisioner "local-exec" {
    command = "kubectl apply -f ${local.istio_alb_ingress_manifest_file} --kubeconfig ${var.kubeconfig_file}"
  }

  depends_on = [
    local_file.istio_alb_ingress,
    helm_release.istio,
    helm_release.aws_alb_ingress,
  ]
}

resource "null_resource" "istio_alb_ingress_destroy" {
  # We want to destroy the ingress created by null_resource.istio_ingress
  count = local.install_istio_alb_ingress ? 1 : 0

  provisioner "local-exec" {
    when    = destroy
    command = "kubectl delete -f ${local.istio_alb_ingress_manifest_file} --kubeconfig ${var.kubeconfig_file}"
  }

  depends_on = [
    local_file.istio_alb_ingress,
    helm_release.istio,
    helm_release.aws_alb_ingress,
  ]
}

