# 2019/10/18 - ref: https://docs.aws.amazon.com/eks/latest/userguide/cni-metrics-helper.html
locals {
  install_aws_k8s_cni_metrics = var.is_eks && var.install_aws_k8s_cni_metrics

  aws_k8s_cni_metrics_manifest_file = join("", local_file.aws_k8s_cni_metrics_manifest.*.filename)

  cni_metrics_namespace         = "kube-system"
  cni_metrics_name              = "cni-metrics-helper"
  cni_metrics_iam_resource_name = "k8s-${local.cni_metrics_name}-ctrl-${var.project}-${var.environment}-${var.aws_region}"
}

data "http" "aws_k8s_cni_metrics_manifest" {
  count = local.install_aws_k8s_cni_metrics ? 1 : 0

  url = var.aws_k8s_cni_metrics_manifest_url
}

resource "local_file" "aws_k8s_cni_metrics_manifest" {
  count = local.install_aws_k8s_cni_metrics ? 1 : 0

  filename = "${var.config_output_path}/${var.aws_k8s_cni_metrics_manifest_file}"
  content  = join("", data.http.aws_k8s_cni_metrics_manifest.*.body)
}

resource "null_resource" "install_aws_k8s_cni_metrics" {
  count = local.install_aws_k8s_cni_metrics ? 1 : 0

  provisioner "local-exec" {
    when    = create
    command = "kubectl apply -f ${local.aws_k8s_cni_metrics_manifest_file} --kubeconfig ${var.kubeconfig_file}"
  }

  triggers = {
    calico_config = sha1(join("", local_file.aws_k8s_cni_metrics_manifest.*.content))
  }
}

resource "null_resource" "install_aws_k8s_cni_metrics_destroy" {
  count = local.install_aws_k8s_cni_metrics ? 1 : 0

  provisioner "local-exec" {
    when = destroy
    # NB: Using local.aws_k8s_cni_metrics_manifest_file here instead of the join(...) would make more sense,
    #     however that introduces a cycle when destroying:
    #
    # Error: Cycle: module.kubernetes_services.null_resource.install_aws_k8s_cni_metrics_destroy[0] (destroy), module.kubernetes_services.local_file.aws_k8s_cni_metrics_manifest[0] (destroy), module.kubernetes_services.local.aws_k8s_cni_metrics_manifest_file
    command = "kubectl delete -f ${join("", local_file.aws_k8s_cni_metrics_manifest.*.filename)} --kubeconfig ${var.kubeconfig_file}"
  }
}

module "iam_role_for_cni_metrics" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/iam-role-for-k8s-service-account?ref=v1.0.9"

  project     = var.project
  environment = var.environment
  tags        = var.tags

  create           = local.install_aws_k8s_cni_metrics
  external_trigger = sha1(join("", local_file.aws_k8s_cni_metrics_manifest.*.content))

  aws_account_id    = var.aws_account_id
  aws_region        = var.aws_region
  aws_iam_role_name = local.cni_metrics_iam_resource_name

  kubeconfig_file           = var.kubeconfig_file
  cluster_oidc_provider_url = var.oidc_provider_url

  k8s_service_account_namespace = local.cni_metrics_namespace
  k8s_service_account_name      = local.cni_metrics_name
  k8s_fully_defined_resources   = ["deployment/${local.cni_metrics_name}"]
  aws_iam_role_policy_arns      = [join("", aws_iam_policy.aws_k8s_cni_metrics.*.arn)]
}

resource "aws_iam_policy" "aws_k8s_cni_metrics" {
  count = local.install_aws_k8s_cni_metrics ? 1 : 0

  name   = local.cni_metrics_iam_resource_name
  policy = join("", data.aws_iam_policy_document.aws_k8s_cni_metrics.*.json)
}

data "aws_iam_policy_document" "aws_k8s_cni_metrics" {
  count = local.install_aws_k8s_cni_metrics ? 1 : 0

  statement {
    effect = "Allow"
    actions = [
      "cloudwatch:PutMetricData",
      "eks:GetClusterTag",
    ]
    resources = ["*"]
  }
}
