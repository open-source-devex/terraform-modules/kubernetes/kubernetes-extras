resource "helm_release" "kube2iam" {
  count = local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  name       = "kube2iam"
  repository = "stable"
  chart      = "kube2iam"
  version    = var.kube2iam_helm_chart_version
  namespace  = "kube-system"

  values = [
    <<EOF
verbose: true
rbac:
  create: true
host:
  iptables: true
  interface: eni+
EOF
    ,
  ]

  depends_on = [kubernetes_deployment.tiller_deploy]
}

resource "aws_iam_role_policy_attachment" "eks_workers_assume_role_kube2iam" {
  count = local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  role       = var.workers_iam_role_name
  policy_arn = join("", aws_iam_policy.eks_workers_assume_role_kube2iam.*.arn)
}

resource "aws_iam_policy" "eks_workers_assume_role_kube2iam" {
  count = local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  name   = "eks-workers-assume-role-kube2iam-${var.project}-${var.environment}-${var.aws_region}"
  policy = join("", data.aws_iam_policy_document.eks_workers_assume_role_kube2iam.*.json)
}

data "aws_iam_policy_document" "eks_workers_assume_role_kube2iam" {
  count = local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  statement {
    actions   = ["sts:AssumeRole"]
    effect    = "Allow"
    resources = ["*"]
  }
}

# Use this policy for the assume role trust policy of the pod roles you create
data "aws_iam_policy_document" "eks_workers_assume_role_kube2iam_trust" {
  count = local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.aws_account_id}:role/${var.workers_iam_role_name}"]
    }
  }
}

