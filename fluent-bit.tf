locals {
  fluent_bit_name      = "fluent-bit"
  fluent_bit_namespace = "monitoring"
}

resource "helm_release" "fluent_bit" {
  count = var.install_fluent_bit ? 1 : 0

  name      = local.fluent_bit_name
  chart     = "stable/${local.fluent_bit_name}"
  version   = var.fluent_bit_helm_chart_version
  namespace = local.fluent_bit_namespace

  set {
    name  = "backend.type"
    value = "es"
  }

  set {
    name  = "backend.es.host"
    value = var.fluent_bit_elastic_search_host
  }

  set {
    name  = "backend.es.port"
    value = var.fluent_bit_elastic_search_tls_port
  }

  set {
    name  = "backend.es.tls"
    value = "on"
  }

  set {
    name  = "input.systemd.enabled"
    value = "true"
  }

  set {
    name  = "metrics.enabled"
    value = "true"
  }

  depends_on = [kubernetes_deployment.tiller_deploy]
}
