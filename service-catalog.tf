locals {
  service_catalog_name           = "svc-cat"
  service_catalog_helm_repo_name = "svc-cat"
  install_service_catalog_v2     = length(regexall("^0.2", var.service_catalog_helm_chart_version)) > 0
}

data "helm_repository" "service_catalog" {
  count = var.install_service_broker ? 1 : 0

  name = local.service_catalog_helm_repo_name
  url  = "https://svc-catalog-charts.storage.googleapis.com"
}

resource "helm_release" "service_catalog" {
  count = var.install_service_broker ? 1 : 0

  keyring   = ""
  name      = local.service_catalog_name
  chart     = "${local.service_catalog_helm_repo_name}/catalog"
  namespace = local.service_broker_namespace
  version   = var.service_catalog_helm_chart_version

  dynamic "set" {
    for_each = local.install_service_catalog_v2 ? [1] : []

    content {
      name  = "apiserver.storage.etcd.useEmbedded"
      value = "false"
    }
  }

  dynamic "set" {
    for_each = local.install_service_catalog_v2 ? [1] : []

    content {
      name  = "apiserver.storage.etcd.servers"
      value = "etcd.aws-sb.svc.cluster.local:2379"
    }
  }

  set {
    name  = local.install_service_catalog_v2 ? "apiserver.replicas" : "controllerManager.replicas"
    value = var.service_catalog_replicas
  }

  set {
    name  = local.install_service_catalog_v2 ? "apiserver.resources.limits.memory" : "controllerManager.resources.limits.memory"
    value = var.service_catalog_limits_memory
  }

  set {
    name  = local.install_service_catalog_v2 ?  "apiserver.resources.requests.memory": "controllerManager.resources.requests.memory"
    value = var.service_catalog_requests_memory
  }

  set {
    name  = local.install_service_catalog_v2 ? "apiserver.resources.limits.cpu": "controllerManager.resources.limits.cpu"
    value = var.service_catalog_limits_cpu
  }

  set {
    name  = local.install_service_catalog_v2 ?"apiserver.resources.requests.cpu": "controllerManager.resources.requests.cpu"
    value = var.service_catalog_requests_cpu
  }

  set {
    name  = local.install_service_catalog_v2 ? "apiserver.healthcheck.enabled": "controllerManager.healthcheck.enabled"
    value = var.service_catalog_healthcheck_enabled
  }

  set {
    name  = "asyncBindingOperationsEnabled"
    value = var.service_catalog_enable_async_binding
  }

  set {
    name  = "rbacEnable"
    value = "true"
  }

  depends_on = [
    kubernetes_deployment.tiller_deploy,
    data.helm_repository.service_catalog,
    helm_release.etcd
  ]
}

resource "helm_release" "etcd" {
  count = var.install_service_broker && local.install_service_catalog_v2 ? 1 : 0

  keyring   = ""
  name      = "etcd"
  chart     = "incubator/etcd"
  namespace = local.service_broker_namespace

  set {
    name  = "image.tag"
    value = var.etcd_container_image_tag
  }

  set {
    name  = "image.repository"
    value = "quay.io/coreos/etcd"
  }

  depends_on = [kubernetes_deployment.tiller_deploy]
}
