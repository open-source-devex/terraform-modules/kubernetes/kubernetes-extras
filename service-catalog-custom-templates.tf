locals {
  configure_custom_templates = var.install_service_broker && var.service_catalog_use_custom_templates

  create_custom_template_bucket            = local.configure_custom_templates && var.service_catalog_custom_templates_s3_bucket == ""
  use_existing_bucket_for_custom_templates = local.configure_custom_templates && var.service_catalog_custom_templates_s3_bucket != ""

  custom_template_bucket_name   = local.create_custom_template_bucket ? join("", aws_s3_bucket.aws_sb_templates_created.*.bucket) : join("", data.aws_s3_bucket.aws_sb_templates_existent.*.bucket)
  custom_template_bucket_arn    = local.create_custom_template_bucket ? join("", aws_s3_bucket.aws_sb_templates_created.*.arn) : join("", data.aws_s3_bucket.aws_sb_templates_existent.*.arn)
  custom_template_bucket_region = local.create_custom_template_bucket ? join("", aws_s3_bucket.aws_sb_templates_created.*.region) : join("", data.aws_s3_bucket.aws_sb_templates_existent.*.region)

  custom_template_path          = var.service_catalog_custom_template_path != "" ? var.service_catalog_custom_template_path : "${local.files_path}/aws-sb/templates"
  custom_templates              = formatlist("%s/%s", local.custom_template_path, fileset(local.custom_template_path, "*"))
  custom_template_object_prefix = "templates"
}

resource "aws_s3_bucket" "aws_sb_templates_created" {
  count = local.create_custom_template_bucket ? 1 : 0

  bucket = "aws-sb-templates-${var.aws_region}-${var.environment}-${lower(var.cluster_name)}"
  acl    = "private"

  tags = merge(var.tags, {
    Name = "AWS Service Broker Templates"
  })
}

data "aws_s3_bucket" "aws_sb_templates_existent" {
  count = local.use_existing_bucket_for_custom_templates ? 1 : 0

  bucket = var.service_catalog_custom_templates_s3_bucket
}

resource "aws_s3_bucket_object" "custom_template" {
  count = local.configure_custom_templates ? length(local.custom_templates) : 0

  bucket = local.custom_template_bucket_name
  key    = "/${local.custom_template_object_prefix}/${basename(local.custom_templates[count.index])}"
  source = local.custom_templates[count.index]
  etag   = md5("/${local.custom_template_object_prefix}/${basename(local.custom_templates[count.index])}")

  tags = var.tags
}
