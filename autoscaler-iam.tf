locals {
  autoscaler_iam_resource_name = "k8s-${local.autoscaler_name}-ctrl-${var.project}-${var.environment}-${var.aws_region}"
}

# Policy for autoscaler created by the EKS modyle and used here via `var.autoscaler_iam_policy_arn`

##########
# iam-oidc
##########
# since the helm chart for the autoscaler controller supports service account annotations we do not need to
# use the module that creates the role an annotates the account (since helm will annotate it for us)
module "autoscaler_iam_role" {
  source = "git::https://github.com/miguelaferreira/terraform-aws-iam.git//modules/iam-assumable-role-with-iodc?ref=iam-assumable-role-with-oidc"

  create_role = var.install_autoscaler && local.iam_oidc_mode_enabled

  provider_url = var.oidc_provider_url

  role_name        = local.autoscaler_iam_resource_name
  role_policy_arns = [var.autoscaler_iam_policy_arn]

  oidc_fully_qualified_subjects = ["system:serviceaccount:${local.autoscaler_namespace}:${local.autoscaler_name}"]

  tags = var.tags
}

##########
# kube2iam
##########
resource "local_file" "autoscaler_kube2iam" {
  count = var.install_autoscaler && local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  content  = join("", data.template_file.autoscaler.*.rendered)
  filename = "${var.config_output_path}/autoscaler_values.yaml"
}

resource "aws_iam_role" "autoscaler_kube2iam" {
  count = var.install_autoscaler && local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  name               = local.autoscaler_iam_resource_name
  assume_role_policy = join("", data.aws_iam_policy_document.eks_workers_assume_role_kube2iam_trust.*.json)
}

resource "aws_iam_role_policy_attachment" "autoscaler_kube2iam" {
  count = var.install_autoscaler && local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  role       = join("", aws_iam_role.autoscaler_kube2iam.*.name)
  policy_arn = var.autoscaler_iam_policy_arn
}
