locals {
  service_broker_iam_resource_name = "k8s-${local.service_broker_name}-ctrl-${var.project}-${var.environment}-${var.aws_region}"
}

resource "aws_iam_policy" "aws_sb" {
  count = var.install_service_broker ? 1 : 0

  name   = local.service_broker_iam_resource_name
  policy = join("", data.aws_iam_policy_document.aws_sb_role_policy.*.json)
}

data "aws_iam_policy_document" "aws_sb_role_policy" {
  count = var.install_service_broker ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]

    resources = compact([
      "arn:aws:s3:::awsservicebroker/templates/*",
      "arn:aws:s3:::awsservicebroker",
      local.custom_template_bucket_arn
    ])
  }

  statement {
    effect = "Allow"

    actions = [
      "dynamodb:PutItem",
      "dynamodb:GetItem",
      "dynamodb:DeleteItem",
    ]

    resources = [
      join("", aws_dynamodb_table.aws_service_broker.*.arn)
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "ssm:GetParameter",
      "ssm:GetParameters",
    ]

    resources = [
      "arn:aws:ssm:${var.aws_region}:${var.aws_account_id}:parameter/asb-*",
      "arn:aws:ssm:${var.aws_region}:${var.aws_account_id}:parameter/Asb*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "ssm:PutParameter",
    ]

    resources = [
      "arn:aws:ssm:${var.aws_region}:${var.aws_account_id}:parameter/asb-*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "cloudformation:CreateStack",
      "cloudformation:DeleteStack",
      "cloudformation:DescribeStacks",
      "cloudformation:UpdateStack",
      "cloudformation:CancelUpdateStack",
      "cloudformation:DescribeStackEvents",
    ]

    resources = [
      "arn:aws:cloudformation:${var.aws_region}:${var.aws_account_id}:stack/aws-service-broker-*/*",
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "athena:*",
      "dynamodb:*",
      "kms:*",
      "elasticache:*",
      "elasticmapreduce:*",
      "kinesis:*",
      "rds:*",
      "redshift:*",
      "route53:*",
      "s3:*",
      "sns:*",
      "sqs:*",
      "ec2:*",
      "iam:*",
      "lambda:*",
    ]

    resources = [
      "*",
    ]
  }
}

##########
# iam-oidc
##########
module "aws_iam_role_for_service_broker_sa" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/iam-role-for-k8s-service-account?ref=v1.0.11"

  project     = var.project
  environment = var.environment
  tags        = var.tags

  create = var.install_service_broker && local.iam_oidc_mode_enabled

  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region

  kubeconfig_file           = var.kubeconfig_file
  cluster_oidc_provider_url = var.oidc_provider_url

  aws_iam_role_name        = local.service_broker_iam_resource_name
  aws_iam_role_policy_arns = [join("", aws_iam_policy.aws_sb.*.arn)]

  k8s_service_account_namespace    = local.service_broker_namespace
  k8s_service_account_name         = "${local.service_broker_name}-aws-servicebroker-service"
  k8s_fully_defined_resources      = ["deployment/${local.service_broker_name}-aws-servicebroker"]
  k8s_service_account_dependencies = [
    join("", helm_release.aws_service_broker.*.metadata.0.chart),
    join("", helm_release.aws_service_broker.*.metadata.0.revision),
    join("", helm_release.aws_service_broker.*.metadata.0.version),
    sha1(join("", helm_release.aws_service_broker.*.metadata.0.values)),
  ]
}

##########
# kube2iam
##########
resource "aws_iam_role" "aws_sb_kube2iam" {
  count = var.install_service_broker && local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  name               = local.service_broker_iam_resource_name
  assume_role_policy = join("", data.aws_iam_policy_document.eks_workers_assume_role_kube2iam_trust.*.json)
}

resource "aws_iam_role_policy_attachment" "aws_sb_kube2iam" {
  count = var.install_service_broker && local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  role       = join("", aws_iam_role.aws_sb_kube2iam.*.name)
  policy_arn = join("", aws_iam_policy.aws_sb.*.arn)
}
