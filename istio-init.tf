locals {
  istio_init_name = "istio-init"
}

resource "helm_release" "istio_init" {
  count = local.install_istio ? 1 : 0

  name      = local.istio_init_name
  chart     = "${local.istio_helm_repository_name}/${local.istio_init_name}"
  namespace = local.istio_namespace
  version   = var.istio_helm_chart_version

  timeout = 1200

  values = [
    join("", data.template_file.istio_init.*.rendered),
  ]

  depends_on = [
    kubernetes_deployment.tiller_deploy,
  ]
}


data "template_file" "istio_init" {
  count = local.install_istio ? 1 : 0

  template = file("${local.files_path}/templates/istio/${local.istio_init_name}-values.yaml.tpl")

  vars = {
    container_image_tag = var.istio_container_image_tag
  }
}
