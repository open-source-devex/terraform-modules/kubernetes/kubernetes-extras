locals {
  block_worker_instance_metadata_policy_name        = "block-instance-metadata-${var.cluster_name}"
  block_worker_instance_metadata_manifest_file_name = "${local.block_worker_instance_metadata_policy_name}.yaml"
  block_worker_instance_metadata_manifest_file      = join("", local_file.block_worker_instance_metadata.*.filename)
}

data "template_file" "block_worker_instance_metadata" {
  count = local.iam_oidc_mode_enabled && local.calico_with_eks ? 1 : 0

  template = file("${local.files_path}/templates/calico/policies/global-network-policy-block-instance-metadata.yaml.tpl")

  vars = {
    policy_name = local.block_worker_instance_metadata_policy_name
  }
}

resource "local_file" "block_worker_instance_metadata" {
  count = local.iam_oidc_mode_enabled && local.calico_with_eks ? 1 : 0

  filename = "${var.config_output_path}/${local.block_worker_instance_metadata_manifest_file_name}"
  content  = join("", data.template_file.block_worker_instance_metadata.*.rendered)
}

resource "null_resource" "block_worker_instance_metadata" {
  count = local.iam_oidc_mode_enabled && local.calico_with_eks ? 1 : 0

  provisioner "local-exec" {
    command = "kubectl apply -f ${local.block_worker_instance_metadata_manifest_file} --kubeconfig ${var.kubeconfig_file}"
  }

  triggers = {
    policy             = sha1(join("", local_file.block_worker_instance_metadata.*.content))
    changed_on_cluster = data.external.block_worker_instance_metadata[count.index].result["changed"] # join("", ...) does not work here
  }

  depends_on = [null_resource.calico_eks_resources]
}

resource "null_resource" "block_worker_instance_metadata_destroy" {
  count = local.iam_oidc_mode_enabled && local.calico_with_eks ? 1 : 0

  provisioner "local-exec" {
    when    = destroy
    command = "kubectl delete -f ${local.block_worker_instance_metadata_manifest_file} --kubeconfig ${var.kubeconfig_file}"
  }
}

data "external" "block_worker_instance_metadata" {
  count = local.iam_oidc_mode_enabled && local.calico_with_eks ? 1 : 0

  program = ["${local.files_path}/scripts/diff-k8s-resource.sh"]

  query = {
    manifest_file = local.block_worker_instance_metadata_manifest_file
    kubeconfig    = var.kubeconfig_file
  }
}
