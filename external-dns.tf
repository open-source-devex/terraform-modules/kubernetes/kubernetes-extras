locals {
  external_dns_namespace      = "kube-system"
  external_dns_name           = "external-dns"
  external_dns_helm_repo_name = "stable"
}

resource "helm_release" "external_dns" {
  count = var.install_external_dns ? 1 : 0

  keyring   = ""
  name      = local.external_dns_name
  chart     = "${local.external_dns_helm_repo_name}/${local.external_dns_name}"
  namespace = local.external_dns_namespace
  version   = var.external_dns_helm_chart_version

  values = [join("", data.template_file.external_dns_helm_values.*.rendered)]


  set {
    name  = "fullnameOverride"
    value = local.external_dns_name
  }

  set {
    name  = "aws.region"
    value = var.aws_region
  }

  set {
    name  = "aws.zonetype"
    value = "public"
  }

  set {
    name  = "domainFilters"
    value = "{${var.external_dns_domain_filters}}"
  }

  set {
    name  = "txtOwnerId"
    value = local.cluster_name
  }

  dynamic "set" {
    # conditional attribute implemented with list: single value = true | empty = fals
    for_each = var.install_istio ? [1] : []

    content {
      name  = "sources"
      value = "{service,ingress,istio-gateway}"
    }
  }

  depends_on = [kubernetes_deployment.tiller_deploy]
}

data "template_file" "external_dns_helm_values" {
  count = var.install_external_dns ? 1 : 0

  template = file("${local.files_path}/templates/external-dns/helm-chart-values.yaml.tpl")

  vars = {
    dns_record_policy = var.external_dns_record_policy

    kube2iam_pod_annotation = local.install_kube2iam_to_assign_roles_to_pods ? "iam.amazonaws.com/role: '${join("", aws_iam_role.external_dns_kube2iam.*.arn)}'" : ""

    service_account_name                = local.external_dns_name
    service_account_iam_role_annotation = local.iam_oidc_mode_enabled ? "${local.eks_iam_service_account_annotation}: ${module.external_dns_iam_role.this_iam_role_arn}" : ""
  }
}
