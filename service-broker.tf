locals {
  service_broker_namespace      = "aws-sb"
  service_broker_name           = "aws-sb"
  service_broker_helm_repo_name = "aws-sb"
}

resource "helm_release" "aws_service_broker" {
  count = var.install_service_broker ? 1 : 0

  keyring   = ""
  name      = local.service_broker_name
  chart     = "${local.service_broker_helm_repo_name}/aws-servicebroker"
  namespace = local.service_broker_namespace
  version   = var.service_broker_helm_chart_version

  values = [join("", data.template_file.aws_service_broker_helm_values.*.rendered)]

  dynamic "set" {
    # conditional attribute implemented with list: single value = true | empty = fals
    for_each = var.service_catalog_use_custom_templates ? [1] : []
    content {
      name  = "aws.bucket"
      value = local.custom_template_bucket_name
    }
  }

  dynamic "set" {
    # conditional attribute implemented with list: single value = true | empty = fals
    for_each = var.service_catalog_use_custom_templates ? [1] : []
    content {
      name  = "aws.key"
      value = local.custom_template_object_prefix
    }
  }

  dynamic "set" {
    # conditional attribute implemented with list: single value = true | empty = fals
    for_each = var.service_catalog_use_custom_templates ? [1] : []
    content {
      name  = "aws.s3region"
      value = local.custom_template_bucket_region
    }
  }

  depends_on = [
    kubernetes_deployment.tiller_deploy,
    data.helm_repository.aws_service_broker,
    helm_release.service_catalog,
  ]
}

data "helm_repository" "aws_service_broker" {
  count = var.install_service_broker ? 1 : 0

  name = local.service_broker_helm_repo_name
  url  = "https://awsservicebroker.s3.amazonaws.com/charts"
}

data "template_file" "aws_service_broker_helm_values" {
  count = var.install_service_broker ? 1 : 0

  template = file(
    "${local.files_path}/templates/aws-servicebroker/helm-chart-values.yaml.tpl",
  )

  vars = {
    container_image_with_tag = var.service_broker_container_image_with_tag

    aws_region          = var.aws_region
    dynamodb_table_name = join("", aws_dynamodb_table.aws_service_broker.*.id)

    broker_id = local.service_broker_name

    kube2iam_pod_annotation = local.install_kube2iam_to_assign_roles_to_pods ? "iam.amazonaws.com/role: '${join("", aws_iam_role.aws_sb_kube2iam.*.arn)}'" : ""
  }
}

resource "aws_dynamodb_table" "aws_service_broker" {
  count = var.install_service_broker ? 1 : 0

  name           = "${local.service_broker_name}-${var.project}-${var.environment}-${var.aws_region}"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "id"
  range_key      = "userid"

  attribute {
    name = "userid"
    type = "S"
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "type"
    type = "S"
  }

  global_secondary_index {
    name               = "type-userid-index"
    hash_key           = "type"
    range_key          = "userid"
    write_capacity     = 5
    read_capacity      = 5
    projection_type    = "INCLUDE"
    non_key_attributes = ["id", "locked"]
  }

  server_side_encryption {
    enabled = var.enable_kms_encyption
  }

  tags = merge(var.tags, {
    "Name"    = "${local.service_broker_name}-${var.project}-${var.environment}-${var.aws_region}"
    "cluster" = var.cluster_name
  })
}
