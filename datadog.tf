data "template_file" "datadog" {
  count = var.install_datadog ? 1 : 0

  template = file("${local.files_path}/templates/datadog/values.yaml.tmpl")

  vars = {
    region      = var.aws_region
    environment = var.environment
    project     = var.project
  }
}

resource "helm_release" "datadog" {
  count = var.install_datadog ? 1 : 0

  keyring   = ""
  name      = "datadog"
  chart     = "stable/datadog"
  namespace = var.datadog_agent_namespace

  version = var.datadog_helm_chart_version

  values = [
    join("", data.template_file.datadog.*.rendered)
  ]

  set {
    name  = "datadog.apiKey"
    value = var.datadog_api_key
  }
  set {
    name  = "datadog.processAgentEnabled"
    value = "true"
  }
  set {
    name  = "datadog.logsEnabled"
    value = "true"
  }
  set {
    name  = "datadog.logLevel"
    value = "WARN"
  }
  set {
    name  = "datadog.logsConfigContainerCollectAll"
    value = "true"
  }
  set {
    name  = "clusterAgent.enabled"
    value = "true"
  }
  set {
    name  = "clusterAgent.metricsProvider.enabled"
    value = "true"
  }
  set {
    name  = "datadog.leaderElection"
    value = "true"
  }
  set {
    name  = "datadog.collectEvents"
    value = "true"
  }

  depends_on = [
    kubernetes_config_map.dd_agent_config,
    kubernetes_deployment.tiller_deploy,
  ]
}

resource "kubernetes_config_map" "dd_agent_config" {
  count = var.install_datadog ? 1 : 0

  metadata {
    name      = "dd-agent-config"
    namespace = var.datadog_agent_namespace
  }

  data = {
    envoy-config = <<EOF
init_config:

instances:
  # For every instance, you need a `stats_url` and can optionally
  # supply a list of tags. The admin endpoint must be accessible.
  # https://www.envoyproxy.io/docs/envoy/latest/operations/admin

  # Add a `?usedonly` on the end if you wish to ignore
  # unused metrics instead of reporting them as `0`.
  #- stats_url: http://istio-telemetry.istio-system.svc.cluster.local:15000/stats

  #   tags:
  #     - instance:foo

  # You can also whitelist/blacklist metrics using regular expressions.
  # The filtering occurs before tag extraction, so you have the option
  # to have certain tags decide whether or not to keep or ignore metrics.
  # For an exhaustive list of all metrics and tags, see:
  # https://github.com/DataDog/integrations-core/blob/master/envoy/datadog_checks/envoy/metrics.py
  #
  # If you surround patterns by quotes, be sure to
  # escape backslashes with an extra backslash.
  #
  # metric_whitelist:
  #   - cluster\.(in|out)\..*
  #
  # metric_blacklist:
  #   - ^http\..*
  #
  # Results are cached by default to decrease CPU utilization, at
  # the expense of some memory. Disable by setting this to false.
  #
  # cache_metrics: true

  # <<<Note>>> The Envoy admin endpoint does not support auth until:
  # https://github.com/envoyproxy/envoy/issues/2763
  # For an alternative, see:
  # https://gist.github.com/ofek/6051508cd0dfa98fc6c13153b647c6f8
  #
  # If the stats page is behind basic auth:
  # username: USERNAME
  # password: PASSWORD

  # The (optional) verify_ssl parameter instructs the check to validate SSL
  # certificates when connecting to Envoy. Defaulting to true, set to false if
  # you want to disable SSL certificate validation.
  #
  # verify_ssl: true

  # The (optional) skip_proxy parameter bypasses any proxy
  # settings enabled and attempt to reach Envoy directly.
  #
  # skip_proxy: false

  # If you need to specify a custom timeout in seconds (default is 20):
  # timeout: 20
EOF

    istio-config = <<EOF
init_config:

instances:
  # To enable Istio metrics you must specify the url exposing the API
  #
  # Note for RHEL and SUSE users: due to compatibility issues, the check does not make use of
  # the CPP extension to process Protocol buffer messages coming from the api. Depending
  # on the metrics volume, the check may run very slowly.
  - istio_mesh_endpoint: http://istio-telemetry.istio-system.svc.cluster.local:42422/metrics
    mixer_endpoint: http://istio-telemetry.istio-system.svc.cluster.local:9093/metrics
    send_histograms_buckets: true
    # tags:
    #   - optional_tag1
    #   - optional_tag2
EOF

  }
}

resource "helm_release" "kube_state_metrics" {
  count = var.install_datadog ? 1 : 0

  name      = "ksm"
  keyring   = ""
  chart     = "stable/kube-state-metrics"
  namespace = "kube-system"

  depends_on = [kubernetes_deployment.tiller_deploy]
}

