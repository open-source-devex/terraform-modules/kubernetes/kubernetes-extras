locals {
  alb_ingress_iam_resource_name = "k8s-${local.alb_ingress_name}-ctrl-${var.project}-${var.environment}-${var.aws_region}"
}

resource "aws_iam_policy" "aws_alb_ingress" {
  count = var.install_alb_ingress ? 1 : 0

  name   = local.alb_ingress_iam_resource_name
  policy = join("", data.aws_iam_policy_document.aws_alb_ingress_role_policy.*.json)
}

data "aws_iam_policy_document" "aws_alb_ingress_role_policy" {
  count = var.install_alb_ingress ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "acm:DescribeCertificate",
      "acm:ListCertificates",
      "acm:GetCertificate",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:CreateSecurityGroup",
      "ec2:CreateTags",
      "ec2:DeleteTags",
      "ec2:DeleteSecurityGroup",
      "ec2:DescribeAccountAttributes",
      "ec2:DescribeAddresses",
      "ec2:DescribeInstances",
      "ec2:DescribeInstanceStatus",
      "ec2:DescribeInternetGateways",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeSubnets",
      "ec2:DescribeTags",
      "ec2:DescribeVpcs",
      "ec2:ModifyInstanceAttribute",
      "ec2:ModifyNetworkInterfaceAttribute",
      "ec2:RevokeSecurityGroupIngress",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "elasticloadbalancing:AddListenerCertificates",
      "elasticloadbalancing:AddTags",
      "elasticloadbalancing:CreateListener",
      "elasticloadbalancing:CreateLoadBalancer",
      "elasticloadbalancing:CreateRule",
      "elasticloadbalancing:CreateTargetGroup",
      "elasticloadbalancing:DeleteListener",
      "elasticloadbalancing:DeleteLoadBalancer",
      "elasticloadbalancing:DeleteRule",
      "elasticloadbalancing:DeleteTargetGroup",
      "elasticloadbalancing:DeregisterTargets",
      "elasticloadbalancing:DescribeListenerCertificates",
      "elasticloadbalancing:DescribeListeners",
      "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeLoadBalancerAttributes",
      "elasticloadbalancing:DescribeRules",
      "elasticloadbalancing:DescribeSSLPolicies",
      "elasticloadbalancing:DescribeTags",
      "elasticloadbalancing:DescribeTargetGroups",
      "elasticloadbalancing:DescribeTargetGroupAttributes",
      "elasticloadbalancing:DescribeTargetHealth",
      "elasticloadbalancing:ModifyListener",
      "elasticloadbalancing:ModifyLoadBalancerAttributes",
      "elasticloadbalancing:ModifyRule",
      "elasticloadbalancing:ModifyTargetGroup",
      "elasticloadbalancing:ModifyTargetGroupAttributes",
      "elasticloadbalancing:RegisterTargets",
      "elasticloadbalancing:RemoveListenerCertificates",
      "elasticloadbalancing:RemoveTags",
      "elasticloadbalancing:SetIpAddressType",
      "elasticloadbalancing:SetSecurityGroups",
      "elasticloadbalancing:SetSubnets",
      "elasticloadbalancing:SetWebACL",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "iam:CreateServiceLinkedRole",
      "iam:GetServerCertificate",
      "iam:ListServerCertificates",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "waf-regional:GetWebACLForResource",
      "waf-regional:GetWebACL",
      "waf-regional:AssociateWebACL",
      "waf-regional:DisassociateWebACL",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "tag:GetResources",
      "tag:TagResources",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "waf:GetWebACL",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "cognito-idp:DescribeUserPool",
      "cognito-idp:DescribeUserPoolClient",
      "cognito-idp:ListUserPoolClients",
      "cognito-idp:ListUserPools",
    ]

    resources = ["*"]
  }
}

##########
# iam-oidc
##########
# since the helm chart for the external-dns controller supports service account annotations we do not need to
# use the module that creates the role an annotates the account (since helm will annotate it for us)
module "alb_ingress_iam_role" {
  source = "git::https://github.com/miguelaferreira/terraform-aws-iam.git//modules/iam-assumable-role-with-iodc?ref=iam-assumable-role-with-oidc"

  create_role = var.install_alb_ingress && local.iam_oidc_mode_enabled

  provider_url = var.oidc_provider_url

  role_name        = local.alb_ingress_iam_resource_name
  role_policy_arns = [join("", aws_iam_policy.aws_alb_ingress.*.arn)]

  oidc_fully_qualified_subjects = ["system:serviceaccount:${local.alb_ingress_namespace}:${local.alb_ingress_name}"]

  tags = var.tags
}

##########
# kube2iam
##########
resource "aws_iam_role" "aws_alb_ingress_kube2iam" {
  count = var.install_alb_ingress && local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  name               = local.alb_ingress_iam_resource_name
  assume_role_policy = join("", data.aws_iam_policy_document.eks_workers_assume_role_kube2iam_trust.*.json)
}

resource "aws_iam_role_policy_attachment" "aws_alb_ingress_kube2iam" {
  count = var.install_alb_ingress && local.install_kube2iam_to_assign_roles_to_pods ? 1 : 0

  role       = join("", aws_iam_role.aws_alb_ingress_kube2iam.*.name)
  policy_arn = join("", aws_iam_policy.aws_alb_ingress.*.arn)
}


