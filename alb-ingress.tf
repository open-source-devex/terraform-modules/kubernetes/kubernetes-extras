locals {
  install_alb_ingress = var.install_helm && var.install_alb_ingress

  alb_ingress_name      = "alb-ingress"
  alb_ingress_namespace = "kube-system"
}

resource "helm_release" "aws_alb_ingress" {
  count = local.install_alb_ingress ? 1 : 0

  keyring   = ""
  name      = local.alb_ingress_name
  namespace = local.alb_ingress_namespace
  chart     = "incubator/aws-alb-ingress-controller"

  values = [
    join("", data.template_file.aws_alb_ingress.*.rendered),
  ]

  set {
    name  = "nameOverride"
    value = local.alb_ingress_name
  }

  depends_on = [kubernetes_deployment.tiller_deploy]
}

data "template_file" "aws_alb_ingress" {
  count = local.install_alb_ingress ? 1 : 0

  template = file("${local.files_path}/templates/alb-ingress/values.yaml.tpl")

  vars = {
    aws_region   = var.aws_region
    cluster_name = var.cluster_name
    vpc_id       = var.vpc_id

    auto_discover_aws_region = false
    auto_discover_vpc        = false

    container_image_tag = var.alb_ingress_container_image_tag

    service_account_name                = local.alb_ingress_name
    service_account_iam_role_annotation = local.iam_oidc_mode_enabled ? "${local.eks_iam_service_account_annotation}: ${module.alb_ingress_iam_role.this_iam_role_arn}" : ""

    kube2iam_pod_annotation = local.install_kube2iam_to_assign_roles_to_pods ? "iam.amazonaws.com/role: '${join("", aws_iam_role.autoscaler_kube2iam.*.arn)}'" : ""

    resource_limits_cpu   = var.alb_ingress_resource_limits_cpu
    resource_limits_mem   = var.alb_ingress_resource_limits_mem
    resource_requests_cpu = var.alb_ingress_resource_requests_cpu
    resource_requests_mem = var.alb_ingress_resource_requests_mem
  }
}
