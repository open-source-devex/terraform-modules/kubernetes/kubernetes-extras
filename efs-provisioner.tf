resource "aws_efs_file_system" "efs_file_system" {
  count = var.install_efs_provisioner ? 1 : 0

  creation_token   = var.project
  performance_mode = "generalPurpose"
  throughput_mode  = "bursting"

  tags = {
    Name    = var.project
    Cluster = var.cluster_name
  }
}

// security.tf
resource "aws_security_group" "efs_sg" {
  count = var.install_efs_provisioner ? 1 : 0

  name   = "ingress-efs-k8s-sg"
  vpc_id = var.vpc_id

  // NFS
  ingress {
    security_groups = [var.worker_nodes_security_group_id]
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
  }

  // Terraform removes the default rule
  egress {
    security_groups = [var.worker_nodes_security_group_id]
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
  }

  tags = {
    Name = "ingress-efs-k8s-sg"
  }
}

resource "aws_efs_mount_target" "efs_file_system_mount" {
  count = length(var.worker_subnet_ids)

  file_system_id  = aws_efs_file_system.efs_file_system[0].id
  subnet_id       = var.worker_subnet_ids[count.index]
  security_groups = [aws_security_group.efs_sg[0].id]
}

resource "helm_release" "efs_provisioner" {
  count = var.install_efs_provisioner ? 1 : 0

  keyring   = ""
  name      = "efs-provisioner"
  chart     = "stable/efs-provisioner"
  namespace = "kube-system"

  set {
    name  = "efsProvisioner.efsFileSystemId"
    value = aws_efs_file_system.efs_file_system[0].id
  }

  set {
    name  = "efsProvisioner.awsRegion"
    value = var.aws_region
  }


  depends_on = [
    aws_efs_mount_target.efs_file_system_mount,
    kubernetes_deployment.tiller_deploy,
  ]
}

