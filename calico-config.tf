resource "kubernetes_cluster_role_binding" "typha_cpha" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name = "typha-cpha"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"

    kind = "ClusterRole"
    name = "typha-cpha"
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = "typha-cpha"
    namespace = "kube-system"
  }
}

resource "kubernetes_cluster_role" "typha_cpha" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name = "typha-cpha"
  }

  rule {
    api_groups = [""]
    resources  = ["nodes"]
    verbs      = ["list"]
  }
}

resource "kubernetes_config_map" "calico_typha_horizontal_autoscaler" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name      = "calico-typha-horizontal-autoscaler"
    namespace = "kube-system"
  }

  data = {
    ladder = <<EOF
{
  "coresToReplicas": [],
  "nodesToReplicas":
  [
    [1, 1],
    [10, 2],
    [100, 3],
    [250, 4],
    [500, 5],
    [1000, 6],
    [1500, 7],
    [2000, 8]
  ]
}
EOF

  }
}

resource "kubernetes_deployment" "calico_typha_horizontal_autoscaler" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name      = "calico-typha-horizontal-autoscaler"
    namespace = "kube-system"

    labels = {
      k8s-app = "calico-typha-autoscaler"
    }
  }

  spec {
    selector {
      match_labels = {
        k8s-app = "calico-typha-autoscaler"
      }
    }

    template {
      metadata {
        namespace = ""

        labels = {
          k8s-app = "calico-typha-autoscaler"
        }
      }

      spec {
        container {
          image = "k8s.gcr.io/cluster-proportional-autoscaler-amd64:1.1.2"
          name  = "autoscaler"

          command = [
            "/cluster-proportional-autoscaler",
            "--namespace=kube-system",
            "--configmap=calico-typha-horizontal-autoscaler",
            "--target=deployment/calico-typha",
            "--logtostderr=true",
            "--v=2",
          ]

          resources {
            requests {
              cpu = "10m"
            }

            limits {
              cpu = "10m"
            }
          }
        }

        service_account_name = kubernetes_service_account.typha_cpha[0].metadata[0].name
      }
    }
  }
}

resource "kubernetes_role" "typha_cpha" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name      = "typha-cpha"
    namespace = "kube-system"
  }

  rule {
    api_groups = [""]
    resources  = ["configmaps"]
    verbs      = ["get"]
  }

  rule {
    api_groups = ["extensions"]
    resources  = ["deployments/scale"]
    verbs      = ["get", "update"]
  }
}

resource "kubernetes_service_account" "typha_cpha" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name      = "typha-cpha"
    namespace = "kube-system"
  }
}

resource "kubernetes_role_binding" "typha_cpha" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name      = "typha-cpha"
    namespace = "kube-system"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "typha-cpha"
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = "typha-cpha"
    namespace = "kube-system"
  }
}

resource "kubernetes_service" "calico_typha" {
  count = local.calico_without_eks ? 1 : 0

  metadata {
    name      = "calico-typha"
    namespace = "kube-system"

    labels = {
      k8s-app = "calico-typha"
    }
  }

  spec {
    port {
      port        = 5473
      protocol    = "TCP"
      target_port = "calico-typha"
      name        = "calico-typha"
    }

    selector = {
      k8s-app = "calico-typha"
    }
  }
}

