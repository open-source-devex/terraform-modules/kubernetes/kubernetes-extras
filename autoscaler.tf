locals {
  autoscaler_namespace = "kube-system"
  autoscaler_name      = "cluster-autoscaler"
}

resource "helm_release" "autoscaler" {
  count = var.install_autoscaler ? 1 : 0

  keyring   = ""
  name      = local.autoscaler_name
  namespace = local.autoscaler_namespace
  chart     = "stable/${local.autoscaler_name}"
  version   = var.autoscaler_helm_chart_version

  force_update = var.hlem_release_force_update

  values = [
    join("", data.template_file.autoscaler.*.rendered),
  ]
  set {
    name  = "image.tag"
    value = var.autoscaler_container_image_tag
  }

  depends_on = [kubernetes_deployment.tiller_deploy]
}

data "template_file" "autoscaler" {
  count = var.install_autoscaler ? 1 : 0

  template = file("${local.files_path}/templates/autoscaler/values.yaml.tmpl")

  vars = {
    name_override = local.autoscaler_name

    region       = var.aws_region
    cluster_name = local.cluster_name

    kube2iam_pod_annotation = local.install_kube2iam_to_assign_roles_to_pods ? "iam.amazonaws.com/role: '${join("", aws_iam_role.autoscaler_kube2iam.*.arn)}'" : ""

    service_account_iam_role_annotation = local.iam_oidc_mode_enabled ? "${local.eks_iam_service_account_annotation}: ${module.autoscaler_iam_role.this_iam_role_arn}" : ""
  }
}
